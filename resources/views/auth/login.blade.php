@extends('layouts.app')
@section('title', 'Login')
@section('content')
    <div class="login-container">
        <form action="{{ route('login') }}" method="POST" class="form-login">

            @csrf
            <ul class="login-nav">
                <li class="login-nav__item active">
                    <a href="login">Login</a>
                </li>
                <li class="login-nav__item">
                    <a href="register">Register</a>
                </li>
            </ul>
            <label for="email" class="login__label">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="login__input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
            @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror


            <label for="password" class="login__label">{{ __('Password') }}</label>
            <input id="password" type="password" class="login__input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror


            <label for="remember" class="login__label--checkbox">
                <input id="remember" name="remember" type="checkbox" class="login__input--checkbox" {{ old('remember') ? 'checked' : '' }}/>
                {{ __('Remember Me') }}
            </label>


            <button class="login__submit" type="submit">{{ __('Login') }}</button>
            @if (Route::has('password.request'))
                <a class="login__forgot" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </form>
{{--        <a href="#" class="login__forgot">Forgot Password?</a>--}}
    </div>
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
                        </div>

                        <div class="form-group row">
<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
                        </div>

                        <div class="form-group row">
<div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
