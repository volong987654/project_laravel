<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Role;
use App\Traits\HandleImageTrait;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Intervention\Image\File;

class UsersController extends Controller
{
    use HandleImageTrait;

    protected $path;

    protected $userModel;
    protected $roleModel;

    public function __construct(User $user, Role $role)
    {
        $this->userModel = $user;
        $this->path = 'upload/';
        $this->roleModel = $role;
    }


    public function index()
    {

        $users = $this->userModel->with('roles')->latest('id')->paginate(6);

        return view('manager.users.index')->with('users',$users);
    }


    public function create()
    {
        $roles = $this->roleModel->where('name','!=','super-admin')->get();

        return view('manager.users.create', compact('roles'));
    }


    public function store(CreateUserRequest $request)
    {
        $image = $request->file('image');
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->saveImage($image, $this->path);
        $dataCreate['password'] = Hash::make($request->input('password'));

        $user = $this->userModel->create($dataCreate);
        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('message', 'Tao user thanh cong');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = $this->userModel->with('roles')->findOrFail($id);
        $roles = $this->roleModel->where('name', '!=', 'super-admin')->get();
        $listRoleIds = $user->roles->pluck('id')->toArray();

        return view('manager.users.edit')->with(['user'=> $user, 'roles' =>$roles, 'listRoleIds' =>$listRoleIds]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $image = $request->file('image');

        $dataUpdate = $request->all();

        $dataUpdate['image'] = $this->updateImage($image, $this->path, $user->image);

        $user->update($dataUpdate);
        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('message', 'Edit user thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UpdateUserRequest $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
//        $image = $request->file('image');
//        $this ->deleteImage($image, $this->path);
        $user->delete();
        return redirect('manage/users')->with('message','Delete user');
    }
}
