<?php

namespace App\Http\Controllers\Manager;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Models\Product;

class categoryController extends Controller
{
    protected $categoryModel;
    protected $productModel;

    /**
     * CategoryController constructor.
     * @param $categoryModel
     */
    public function __construct(Category $categoryModel,Product $product)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
    }


    public function index()
    {
        $categories = $this->categoryModel->with(['childCategories', 'parentCategories'])
            ->latest('id')->paginate(5);

        return view('manager.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories =  $this->categoryModel->get(['id', 'name']);

        return view('manager.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataCreate =  $request->all();
        $this->categoryModel->create($dataCreate);

        return redirect()->route('categories.index')->with('message', 'tao category thanh cong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->categoryModel->findOrFail($id);
        return response()->json([
            'status'=>200,
            'data' =>new CategoryResource($category)
        ]);
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $categories = $this->categoryModel->get(['id', 'name']);
        return view('manager.category.edit')->with(['category'=>$category,'categories'=>$categories]);
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $dataUpdate = $request->all();
        $category->update($dataUpdate);

        return redirect()->route('categories.index')->with('message', 'Edit category thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categoryModel->findOrFail($id);
        $category->delete();
        return redirect('manage/categories')->with('message','Delete category');
    }
}
