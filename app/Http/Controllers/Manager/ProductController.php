<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Traits\HandleImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    use HandleImageTrait;

    protected $path;
    protected $categoryModel;
    protected $productModel;



    public function __construct(Product $product,Category $categoryModel)
    {

        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
        $this->path = 'upload/';
    }

    public function index()
    {
//        $products = $this->productModel->paginate(3);
//
//        return view('manager.product.index')->with( 'products',$products);
        $products =  $this->productModel->with(['categories'])
            ->latest('id')->paginate(5);

        return view('manager.product.index', compact('products'));

    }

    public function create()
    {
        $categories =  $this->categoryModel->get(['id', 'name']);

        return view('manager.product.create', compact('categories'));


    }


    public function store(Request  $request)
    {
        $image = $request->file('image');
        $dataCreate =  $request->all();
        $dataCreate['image'] = $this->saveImage($image, $this->path);
        $dataCreate['password'] = Hash::make($request->input('password'));
        $product = $this->productModel->create($dataCreate);

        $product->assignCategories($dataCreate['category_ids']);

        return redirect()->route('product.index')->with('message', 'tao category thanh cong');

    }


    public function show($id)
    {


    }


    public function edit($id){
        $product = $this->productModel->with(['categories'])->findOrFail($id);
        $categories = $this->categoryModel->get();
        $listCategoryIds = $product->categories->pluck('id')->toArray();
        return view('manager.product.edit')->with(['product'=> $product,'categories'=>$categories,'listCategoryIds'=>$listCategoryIds]);
    }




    public function update(Request $request, $id)
    {
        $product = $this->productModel->findOrFail($id);
        $image = $request->file('image');
        $dataUpdate = $request->all();
        $dataUpdate['image'] = $this->updateImage($image, $this->path, $product->image);
        $product->update($dataUpdate);
        $product->assignCategories($request -> listCategoryIds);
        return redirect()->route('products.index')->with('message', 'Edit product thanh cong');
    }

    public function destroy($id)
    {
        $product = $this->productModel->findOrFail($id);
        $product->delete();

        //Thực hiện chuyển trang
        return redirect()->route('product.index')->with('message', 'Delete product success');
    }
}
