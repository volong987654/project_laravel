<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected Category $categoryModel;
    protected Product $productModel;
    protected Cart $cart;
    protected Coupon $coupon;
    protected Order $order;
    protected OrderDetail $orderDetail;

    /**
     * CategoryController constructor.
     * @param Category $categoryModel
     * @param Product $product
     * @param Cart $cart
     * @param Coupon $coupon
     * @param Order $order
     * @param OrderDetail $orderDetail
     */
    public function __construct(Category $categoryModel,Product $product, Cart $cart, Coupon $coupon, Order $order, OrderDetail $orderDetail)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
        $this->cart = $cart;
        $this->coupon = $coupon;
        $this->orderDetail = $orderDetail;
        $this->order = $order;
    }

    public function index(Request $request)
    {
        $orders = $this->order->with(['orderDetails'])->latest('id')->paginate(10);

        return view('manage.order.index', compact('orders'));
    }

    public function show($id)
    {
        $order = $this->order->with(['orderDetails', 'orderDetails.product'])->findOrFail($id);

        return view('manage.order.show', compact('order'));
    }
}
