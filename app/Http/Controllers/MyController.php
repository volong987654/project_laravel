<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function XinChao(){
        echo "Chao cac ban";
    }

    public function KhoaHoc($ten)
    {
        echo "Khoa hoc: ".$ten;
        return redirect()->route('MyRoute');
    }

    public function GetURL(Request $request){
        //return $request->path();
        //return $request->url();
        //return $request->is('admin/*'); //Kiem tra URL co chua chuoi 'admin/' hay k
        /*if($request->isMethod('get'))
            echo "Phuong thuc Get";
        else
            echo "Khong phai phuong thuc Get";*/
        if($request->is('My*'))
            echo "Co My";
        else
            echo "Khong co My";
    }

    public function postForm1(Request $request){
//        echo "Tên của bạn là: ";
//        echo $request->HoTen;
        if($request->has('Tuoi'))
            echo "Co tham so";
        else
            echo "Khong co tham so";
    }
    /* $request->has('name'); Kiểm tra tham số 'name' có tồn tại không
    $request->input('id'); Nhận dữ liệu từ thẻ <input name='id'>
    $request->input('products.name'); Nhận dữ liệu từ mảng
    $request->input('user.name'); Nhận dữ liệu từ JSON dạng mảng
    $request->all(); Nhận hết dữ liệu, lưu thành dạng mảng
    $request->only('age'); Chỉ nhận tham số age
    $request->except('age'); Nhận tất cả tham số ngoại trừ tham số age*/
}
