<?php
/*
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('KhoaHoc', function(){
    return "Xin chào các bạn !";
});

Route::get('GiaBao/Laravel',function(){
    echo "<h1>Khoa học - Laravel</h1>";
});

// Truyen tham so
Route::get('HoTen/{ten}',function($ten){
    return "Ten cua ban la:".$ten;
});

Route::get('Laravel/{ngay}', function($ngay){
    echo "Gia Bao:".$ngay;
})->where(['ngay'=>'[a-zA-Z]+']);

//Dinh danh
Route::get('Route1', ['as'=>'MyRoute', function(){
    echo "Xin chao cac ban";
}]);

Route::get('Route2', function(){
    echo "Day la route 2";
})->name('MyRoute2');

Route::get('GoiTen', function(){
    return redirect()->route('MyRoute2');
});

Route::get('GoiTenRoute1', function(){
    return redirect()->route('Route1'); //ERROR Route [Route1] not defined
});
//Group
Route::group(['prefix'=>'MyGroup'], function(){
    Route::get('User1', function(){
        echo "User1";
    });
    Route::get('User2', function(){
        echo "User2";
    });
    Route::get('User3', function(){
        echo "User3";
    });
});

//Goi Controller
Route::get('GoiController','MyController@XinChao');

Route::get('ThamSo/{ten}','MyController@KhoaHoc');

Route::get('MyRequest','MyController@GetURL');

//Gui nhan du lieu voi request
Route::get('getForm', function (){
    return view('postForm');
});

Route::post('postForm',['as'=>'postForm1','uses'=>'MyController@postForm1']);*/
