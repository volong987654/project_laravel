<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';


    protected $fillable = [
        'name', 'price', 'sale','image'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    public function assignCategories($categoryIds = [])
    {
        return $this->categories()->sync($categoryIds);
    }

    public function addCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }

    public function removeCategories($categoryIds)
    {
        return  $this->categories()->detach($categoryIds);
    }
}
