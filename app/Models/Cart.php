<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = [
        'product_id',
        'user_id',
        'quantity'
    ];

    public function getCartBy($data = [])
    {
        return $this->with('product')->where('product_id', $data['product_id'])->where('user_id', $data['user_id'])->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public $timestamps = false;
}
