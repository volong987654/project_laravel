<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $table = 'coupons';

    protected $fillable = [
        'code',
        'expiry_date',
        'quantity',
        'type',
        'value',
    ];

}
