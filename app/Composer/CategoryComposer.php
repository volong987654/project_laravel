<?php


namespace App\Composer;


use App\Models\Cart;
use App\Models\Category;
use Illuminate\View\View;

class CategoryComposer
{
    protected Category $category;

    /**
     * CartComposer constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }


    public function compose(View $view)
    {
        $categories = $this->category->get(['name','id']);
        $view->with(['categories' => $categories]);
    }
}
